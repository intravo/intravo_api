DOCKER_COMPOSE=docker-compose
SYMFONY_CONSOLE=exec php bin/console

### Symfony CMD ###

me:
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) make:entity

dsu:
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) doctrine:schema:update --force

dmm:
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) doctrine:schema:drop --force
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) doctrine:schema:update --force
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) doctrine:migrations:migrate

dmg:
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) doctrine:migrations:generate

cc:
	$(DOCKER_COMPOSE) $(SYMFONY_CONSOLE) cache:clear