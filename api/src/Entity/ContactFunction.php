<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactFunctionRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"contact_function", "contact_function-read"}},
 *     "denormalization_context"={"groups"={"contact_function", "contact_function-write"}}
 * })
 */
class ContactFunction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"contact","contact_function", "customer"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"contact","contact_function", "customer"})
     */
    private $title;

    /**
     * @var Contact
     *
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="function")
     */
    private $contact;

    /**
     * ContactFunction constructor.
     */
    public function __construct()
    {
        $this->contact = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ContactFunction
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
}
