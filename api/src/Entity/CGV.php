<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CGVRepository")
 *
 * @ApiResource(
 *     attributes={
 *        "normalization_context"={"groups"={"cgv", "cgv-read"}},
 *        "denormalization_context"={"groups"={"cgv", "cgv-write"}}
 * })
 */
class CGV
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"cgv", "quotation"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"cgv", "quotation"})
     */
    private $title;

    /**
     * @var Quotation
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="cgv")
     * @Groups({"cgv"})
     */
    private $quotation;

    /**
     * CGV constructor.
     */
    public function __construct()
    {
        $this->quotation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return CGV
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }
}