<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 * @ApiResource(
 *     attributes={
 *         "normalization_context"={
 *              "groups"={"role", "role-read"},
 *              "enable_max_depth" = "true",
 *         },
 *         "denormalization_context"={"groups"={"role", "role-write"}}
 *     }
 * )
 */
class Role
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"role", "user"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"role", "user"})
     */
    private $title;


    /**
     * @ORM\ManyToMany(targetEntity="Module", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="role_module",
     *     joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="module_id", referencedColumnName="id")}
     * )
     * @ApiSubresource
     * @MaxDepth(20)
     * @Groups({"role", "module", "user"})
     */
    private $modules;

    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->modules = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Role
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * @param Module $modules
     * @return $this
     */
    public function addModule(Module $modules)
    {
        $this->modules[] = $modules;

        return $this;
    }

    /**
     * @param ArrayCollection $modules
     * @return $this
     */
    public function setModules(ArrayCollection $modules)
    {
        $this->modules = $modules;

        return $this;
    }

    /**
     * @param Module $module
     * @return $this
     */
    public function removeModule(Module $module)
    {
        $this->modules->removeElement($module);

        return $this;
    }

}
