<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Customer;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Filter\OrSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 *
 * @ApiFilter(OrSearchFilter::class, properties={
 *     "firstName": "ipartial",
 *     "lastName": "ipartial",
 *     "phone": "ipartial",
 *     "mobilePhone": "ipartial",
 *     "email": "ipartial",
 * })
 * @ApiFilter(OrderFilter::class, properties={
 *     "name",
 *     "lastName",
 *     "phone",
 *     "mobilePhone",
 *     "email"
 *     },
 *     arguments={"orderParameterName"="order"
 * })
 * @ApiResource(
 *     attributes={
 *         "normalization_context"={
 *              "groups"={"contact", "contact-read"},
 *              "enable_max_depth" = "true",
 *         },
 *         "denormalization_context"={"groups"={"contact", "contact-write"}}
 *     },
 * )
 * @ApiFilter(NumericFilter::class, properties={"customer.id"})
 */
class Contact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"contact", "business", "customer"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=255)
     * @Groups({"contact", "business", "customer"})
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255)
     * @Groups({"contact", "customer", "business"})
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     * @Groups({"contact", "business", "customer"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="MobilePhone", type="string", length=255, nullable=true)
     * @Groups({"contact", "business", "customer"})
     */
    private $mobilePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="Email", type="string", length=255, nullable=true)
     * @Groups({"contact", "business", "customer"})
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="ContactFunction", inversedBy="contact")
     * @Groups({"contact", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $function;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="contact")
     * @Groups({"contact"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $customer;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Business", mappedBy="comContact")
     * @Groups({"customer"})
     */
    private $comContact;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Business", mappedBy="techContact")
     * @Groups({"customer"})
     */
    private $techContact;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
        $this->comContact = new ArrayCollection();
        $this->techContact = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return Contact
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param $customer
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param mixed $function
     * @return Contact
     */
    public function setFunction(ContactFunction $function)
    {
        $this->function = $function;
        return $this;
    }

    /**
     * @return Business
     */
    public function getComContact()
    {
        return $this->comContact;
    }

    /**
     * @return Business
     */
    public function getTechContact()
    {
        return $this->techContact;
    }

}