<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UnitRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"unit", "unit-read"}, "enable_max_depth" = "true"},
 *     "denormalization_context"={"groups"={"unit", "unit-write"}}
 * })
 */
class Unit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"unit", "quotation_line", "quotation"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"unit", "quotation_line", "quotation"})
     */
    private $title;

    /**
     * @var QuotationLine
     *
     * @ORM\OneToMany(targetEntity="QuotationLine", mappedBy="unit")
     * @Groups({"unit"})
     * @MaxDepth(2)
     */
    private $quotationLine;

    /**
     * Unit constructor.
     */
    public function __construct()
    {
        $this->quotationLine = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Unit
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getQuotationLine()
    {
        return $this->quotationLine;
    }
}
