<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Filter\OrSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerRepository")
 * @ApiFilter(OrSearchFilter::class, properties={"name": "ipartial", "siret": "ipartial", "postalCode": "ipartial"})
 * @ApiFilter(OrderFilter::class, properties={"name", "postalCode"}, arguments={"orderParameterName"="order"})
 * @ApiResource(
 *   attributes={
 *       "order"={"id": "DESC"},
 *       "pagination_items_per_page"=20,
 *       "normalization_context"={"groups"={"customer", "customer-read"}},
 *       "denormalization_context"={"groups"={"customer", "customer-write"}}
 *   }
 * )
 *
 */
class Customer
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"customer", "business", "contact", "quotation"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="socialreason", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $socialReason;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="addressComplement", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $addressComplement;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $postalCode;
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="memo", type="text", nullable=true)
     * @Groups({"customer", "business", "contact", "quotation", "quotation_line"})
     */
    private $memo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean",  nullable=true)
     * @Groups({"customer", "business", "contact", "quotation_line"})
     */
    private $isActive;

    /**
     * @var Groupe
     *
     * @ORM\ManyToOne(targetEntity="Groupe", inversedBy="customer")
     * @Groups({"customer", "business", "contact", "quotation"})
     */
    private $groupe;

    /**
     * @var Contact
     *
     * @ORM\OneToMany(targetEntity="Contact", mappedBy="customer")
     * @Groups({"customer", "business"})
     */
    private $contact;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Business", mappedBy="customer")
     * @Groups({"customer", "business"})
     */
    private $business;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="customer")
     * @Groups({"customer", "business"})
     */
    private $quotation;

    /**
     * Customer constructor.
     */
    public function __construct()
    {
        $this->contact = new ArrayCollection();
        $this->business = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Customer
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getSocialReason()
    {
        return $this->socialReason;
    }

    /**
     * @param string $socialReason
     * @return Customer
     */
    public function setSocialReason($socialReason)
    {
        $this->socialReason = $socialReason;
        return $this;
    }

    /**
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param string $siret
     * @return Customer
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressComplement()
    {
        return $this->addressComplement;
    }

    /**
     * @param string $addressComplement
     * @return Customer
     */
    public function setAddressComplement($addressComplement)
    {
        $this->addressComplement = $addressComplement;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @param string $postalCode
     * @return Customer
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Customer
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     * @return Customer
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getMemo()
    {
        return $this->memo;
    }

    /**
     * @param string $memo
     * @return Customer
     */
    public function setMemo($memo)
    {
        $this->memo = $memo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param mixed $contacts
     * @return Customer
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGroupe()
    {
        return $this->groupe;
    }

    /**
     * @param mixed $groupe
     * @return Customer
     */
    public function setGroupe($groupe)
    {
        $this->groupe = $groupe;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     * @return Customer
     */
    public function setIsActive(bool $isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @return Business
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @return Business
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

}