<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\Contact;
use ApiPlatform\Core\Annotation\ApiFilter;
use App\Filter\OrSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity
 * @ApiResource(
 *     attributes={
 *         "normalization_context"={
 *              "groups"={"user", "user-read"},
 *              "enable_max_depth" = "true",
 *         },
 *         "denormalization_context"={"groups"={"user", "user-write"}}
 *     },
 * )
 * @ORM\AttributeOverrides({
 *      @ORM\AttributeOverride(name="username",column=@ORM\Column(nullable=true)),
 *      @ORM\AttributeOverride(name="usernameCanonical",column=@ORM\Column(nullable=true)),
 *      @ORM\AttributeOverride(name="emailCanonical",column=@ORM\Column(nullable=true)),
 *      @ORM\AttributeOverride(name="roles",column=@ORM\Column(type="array", nullable=true)),
 * })
 * @ApiFilter(
 *     OrSearchFilter::class,
 *     properties={"fullname": "ipartial", "email": "ipartial"}
 * )
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={"username": "exact", "id": "exact"}
 * )
 * @ApiFilter(
 *     OrderFilter::class,
 *     properties={"fullname", "username", "email", "id"}, arguments={"orderParameterName"="order"}
 * )
 */
class UserUtil extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $fullname;

    /**
     * @var string
     *
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $email;

    /**
     * @var string
     *
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $plainPassword;

    /**
     * @var string
     *
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $username;

    /**
     * @var float
     *
     * @ORM\Column(name="costPerYear", type="float", nullable=true)
     * @Groups({"user", "business", "quotation_line_complex"})
     */
    protected $costPerYear;

    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role", cascade={"persist", "merge"})
     * @Groups({"user", "role", "business", "quotation_line_complex"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    protected $role;

    /**
     * @var QuotationLineComplex
     *
     * @ORM\OneToMany(targetEntity="QuotationLineComplex", mappedBy="userUtil")
     */
    private $quotationLineComplex;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Business", mappedBy="comUser")
     * @Groups({"user"})
     */
    private $comUser;

    /**
     * @var Business
     *
     * @ORM\OneToMany(targetEntity="Business", mappedBy="techUser")
     * @Groups({"user"})
     */
    private $techUser;

    /**
     * UserUtil constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->business = new ArrayCollection();
        $this->quotationLineComplex = new ArrayCollection();
        $this->comUser = new ArrayCollection();
        $this->techUser = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set costPerYear
     *
     * @param float $costPerYear
     *
     * @return User
     */
    public function setCostPerYear($costPerYear)
    {
        $this->costPerYear = $costPerYear;

        return $this;
    }

    /**
     * Get costPerYear
     *
     * @return float
     */
    public function getCostPerYear()
    {
        return $this->costPerYear;
    }

    /**
     * @param null|string $fullname
     */
    public function setFullname(?string $fullname): void
    {
        $this->fullname = $fullname;
    }

    /**
     * @return null|string
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * @param UserInterface|null|null $user
     * @return bool
     */
    public function isUser(?UserInterface $user = null): bool
    {
        return $user instanceof self && $user->id === $this->id;
    }


    /**
     * @return mixed
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @return mixed
     */
    public function getQuotationLineComplex()
    {
        return $this->quotationLineComplex;
    }

    /**
     * @return Business
     */
    public function getComUser()
    {
        return $this->comUser;
    }

    /**
     * @return Business
     */
    public function getTechUser()
    {
        return $this->techUser;
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return UserUtil
     */
    public function setRole(Role $role)
    {
        $this->role = $role;
        return $this;
    }
}