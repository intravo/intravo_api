<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\NumericFilter;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuotationLineRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={
 *           "groups"={"quotation_line", "quotation_line-read"},
 *           "enable_max_depth" = "true",
 *     },
 *     "denormalization_context"={"groups"={"quotation_line", "quotation_line-write"}}
 * })
 *
 * @ApiFilter(NumericFilter::class, properties={"quotation.id"})
 *
 */
class QuotationLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"quotation_line", "quotation"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="designation", type="string", length=255)
     * @Groups({"quotation_line", "quotation"})
     */
    private $designation;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     * @Groups({"quotation_line", "quotation"})
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="priceHT", type="float", nullable=true)
     * @Groups({"quotation_line", "quotation"})
     */
    private $priceHT;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     * @Groups({"quotation_line", "quotation"})
     */
    private $position;

    /**
     * @ORM\Column(name="isGift", type="boolean")
     * @Groups({"quotation_line", "quotation"})
     */
    protected $isGift;

    /**
     * @ORM\Column(name="isTitle", type="boolean")
     * @Groups({"quotation_line", "quotation"})
     */
    protected $isTitle;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isOptional", type="boolean")
     * @Groups({"quotation_line", "quotation"})
     */
    private $isOptional;

    /**
     * @var Quotation
     *
     * @ORM\ManyToOne(targetEntity="Quotation", inversedBy="quotationLines")
     * @Groups({"quotation_line"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $quotation;

    /**
     * @var Unit
     *
     * @ORM\ManyToOne(targetEntity="Unit", inversedBy="quotationLine", cascade={"persist", "merge"})
     * @Groups({"quotation_line", "unit", "quotation"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $unit;

    /**
     * QuotationLine constructor.
     */
    public function __construct()
    {
        $this->isGift = false;
        $this->isTitle = false;
    }

    /**
     * @return mixed
     */
    public function getIsGift()
    {
        return $this->isGift;
    }

    /**
     * @param mixed $isGift
     */
    public function setIsGift($isGift): void
    {
        $this->isGift = $isGift;
    }

    /**
     * @return mixed
     */
    public function getIsTitle()
    {
        return $this->isTitle;
    }

    /**
     * @param mixed $isTitle
     */
    public function setIsTitle($isTitle): void
    {
        $this->isTitle = $isTitle;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param string $designation
     * @return QuotationLine
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return QuotationLine
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceHT()
    {
        return $this->priceHT;
    }

    /**
     * @param float $priceHT
     * @return $this
     */
    public function setPriceHT(float $priceHT)
    {
        $this->priceHT = $priceHT;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return QuotationLine
     */
    public function setPosition($position)
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIsOptional()
    {
        return $this->isOptional;
    }

    /**
     * @param bool $isOptional
     * @return QuotationLine
     */
    public function setIsOptional($isOptional)
    {
        $this->isOptional = $isOptional;
        return $this;
    }

    /**
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * @param Quotation $quotation
     * @return QuotationLine
     */
    public function setQuotation(Quotation $quotation)
    {
        $this->quotation = $quotation;
        return $this;
    }

    /**
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param Unit $unit
     * @return QuotationLine
     */
    public function setUnit(Unit $unit)
    {
        $this->unit = $unit;

        return $this;
    }
}
