<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtatRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"etat", "etat-read"}},
 *     "denormalization_context"={"groups"={"etat", "etat-write"}}
 * })
 */
class Etat
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"etat", "quotation", "customer", "business", "quotation_line"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"etat", "quotation", "customer", "business", "quotation_line"})
     */
    private $title;

    /**
     * @var Quotation
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="etat")
     */
    private $quotation;

    /**
     * Etat constructor.
     */
    public function __construct()
    {
        $this->quotation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Etat
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }
}
