<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaymentConditionRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"payment_condition", "payment_condition-read"}},
 *     "denormalization_context"={"groups"={"payment_condition", "payment_condition-write"}}
 * })
 *
 */
class PaymentCondition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"payment_condition", "quotation", "quotation_line"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"payment_condition", "quotation", "quotation_line"})
     */
    private $title;

    /**
     * @var Quotation
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="paymentCondition")
     */
    private $quotation;

    /**
     * PaymentCondition constructor.
     */
    public function __construct()
    {
        $this->quotation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return paymentCondition
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }
}
