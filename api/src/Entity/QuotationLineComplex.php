<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use ApiPlatform\Core\Annotation\ApiSubresource;

/**
 * @ORM\Entity(repositoryClass="App\Repository\QuotationLineComplexRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"quotation_line_complex", "quotation_line_complex-read"}},
 *     "denormalization_context"={"groups"={"quotation_line_complex", "quotation_line_complex-write"}}
 * })
 */
class QuotationLineComplex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"quotation_line_complex", "quotation"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titleClient", type="string", length=255)
     * @Groups({"quotation_line_complex", "quotation"})
     */
    private $titleClient;


    /**
     * @var integer
     *
     * @ORM\Column(name="culomn", type="integer")
     * @Groups({"quotation_line_complex", "quotation"})
     */
    private $culomn;

    /**
     * @var UserUtil
     *
     * @ORM\ManyToOne(targetEntity="UserUtil", inversedBy="quotationLineComplex")
     * @Groups({"quotation_line_complex", "quotation"})
     */
    private $user;

    /**
     * @var Quotation
     *
     * @ORM\ManyToOne(targetEntity="Quotation", inversedBy="quotationLineComplex")
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $quotation;

    /**
     * @ORM\Column(type="float")
     */
    private $tJM;

    /**
     * @var UserUtil
     *
     * @ORM\ManyToOne(targetEntity="UserUtil", inversedBy="quotationLineComplex")
     * @Groups({"quotation"})
     */
    private $userUtil;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titleClient
     *
     * @param string $titleClient
     *
     * @return QuotationLineComplex
     */
    public function setTitleClient($titleClient)
    {
        $this->titleClient = $titleClient;

        return $this;
    }

    /**
     * Get titleClient
     *
     * @return string
     */
    public function getTitleClient()
    {
        return $this->titleClient;
    }


    /**
     * @return int
     */
    public function getCulomn()
    {
        return $this->culomn;
    }

    /**
     * @param int $culomn
     * @return QuotationLineComplex
     */
    public function setCulomn($culomn)
    {
        $this->culomn = $culomn;
        return $this;
    }

    /**
     * @return UserUtil
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserUtil $user
     * @return QuotationLineComplex
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * @param mixed $quotation
     * @return QuotationLineComplex
     */
    public function setQuotation($quotation)
    {
        $this->quotation = $quotation;
        return $this;
    }

    public function getTJM(): ?float
    {
        return $this->tJM;
    }

    public function setTJM(float $tJM): self
    {
        $this->tJM = $tJM;

        return $this;
    }

    /**
     * @return UserUtil
     */
    public function getUserUtil()
    {
        return $this->userUtil;
    }
}
