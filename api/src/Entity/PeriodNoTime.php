<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PeriodNoTimeRepository")
 *
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"period_no_time", "period_no_time-read"}},
 *     "denormalization_context"={"groups"={"period_no_time", "period_no_time-write"}}
 * })
 */
class PeriodNoTime
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"period_no_time"})
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="begin", type="datetime")
     * @Groups({"period_no_time"})
     */
    private $begin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime")
     * @Groups({"period_no_time"})
     */
    private $end;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255)
     * @Groups({"period_no_time"})
     */
    private $reason;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set begin
     *
     * @param \DateTime $begin
     *
     * @return PeriodNoTime
     */
    public function setBegin($begin)
    {
        $this->begin = $begin;

        return $this;
    }

    /**
     * Get begin
     *
     * @return \DateTime
     */
    public function getBegin()
    {
        return $this->begin;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return PeriodNoTime
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * @param string $reason
     * @return PeriodNoTime
     */
    public function setReason($reason)
    {
        $this->reason = $reason;
        return $this;
    }
}
