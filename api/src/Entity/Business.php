<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Filter\OrSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiFilter(OrSearchFilter::class, properties={"libelle": "ipartial", "code": "ipartial"})
 * @ApiFilter(OrderFilter::class, properties={"libelle", "code"}, arguments={"orderParameterName"="order"})
 * @ORM\Entity(repositoryClass="App\Repository\BusinessRepository")
 * @ApiResource(attributes={
 *     "normalization_context"={"groups"={"business", "business-read"}, "enable_max_depth"="true"},
 *     "denormalization_context"={"groups"={"business", "business-write"}}
 * })
 *
 */
class Business
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"business", "quotation", "customer", "quotation_line"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=6, unique=true)
     * @Groups({"business", "quotation", "customer", "quotation_line"})
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     * @Groups({"business", "quotation", "customer", "quotation_line"})
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     * @Groups({"business", "quotation", "customer", "quotation_line"})
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="File", type="string", length=255)
     * @Groups({"business", "quotation", "customer", "quotation_line"})
     */
    private $file;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="business")
     * @Groups({"business", "quotation", "quotation_line"})
     * @MaxDepth(1)
     */
    private $customer;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="comContact")
     * @Groups({"business", "quotation", "quotation_line"})
     * @MaxDepth(1)
     */
    private $comContact;

    /**
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact", inversedBy="techContact")
     * @Groups({"business", "quotation"})
     * @MaxDepth(1)
     */
    private $techContact;

    /**
     * @var UserUtil
     *
     * @ORM\ManyToOne(targetEntity="UserUtil", inversedBy="comUser")
     * @Groups({"business", "quotation"})
     * @MaxDepth(1)
     */
    private $comUser;

    /**
     * @var UserUtil
     *
     * @ORM\ManyToOne(targetEntity="UserUtil", inversedBy="techUser")
     * @Groups({"business", "quotation"})
     * @MaxDepth(1)
     */
    private $techUser;

    /**
     * @var Quotation
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="business")
     * @Groups({"business"})
     */
    private $quotation;

    /**
     * Business constructor.
     */
    public function __construct()
    {
        $this->quotation = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Business
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Business
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Business
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return Business
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     * @return Business
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Contact
     */
    public function getComContact()
    {
        return $this->comContact;
    }

    /**
     * @param Contact $comContact
     * @return Business
     */
    public function setComContact(Contact $comContact)
    {
        $this->comContact = $comContact;
        return $this;
    }

    /**
     * @return Contact
     */
    public function getTechContact()
    {
        return $this->techContact;
    }

    /**
     * @param Contact $techContact
     * @return Business
     */
    public function setTechContact(Contact $techContact)
    {
        $this->techContact = $techContact;
        return $this;
    }

    /**
     * @return UserUtil
     */
    public function getComUser()
    {
        return $this->comUser;
    }

    /**
     * @param UserUtil $comUser
     * @return Business
     */
    public function setComUser(UserUtil $comUser)
    {
        $this->comUser = $comUser;
        return $this;
    }

    /**
     * @return UserUtil
     */
    public function getTechUser()
    {
        return $this->techUser;
    }

    /**
     * @param UserUtil $techUser
     * @return Business
     */
    public function setTechUser(UserUtil $techUser)
    {
        $this->techUser = $techUser;
        return $this;
    }

    /**
     * @return Quotation|ArrayCollection
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

}
