<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeBenefitRepository")
 * @ApiResource(attributes={
 *     "normalization_context"={
 *          "groups"={"type_benefit", "type_benefit-read"},
 *          "enable_max_depth" = "true"
 *     },
 *     "denormalization_context"={"groups"={"type_benefit", "type_benefit-write"}}
 * })
 */
class TypeBenefit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"type_benefit", "quotation", "customer", "business"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"type_benefit", "quotation", "customer", "business"})
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_complex", type="boolean", nullable=true)
     */
    private $isComplex;

    /**
     * @var Quotation
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="benefit")
     */
    private $quotation;

    /**
     * Etat constructor.
     */
    public function __construct()
    {
        $this->quotation = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return TypeBenefit
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * @return bool
     */
    public function isIsComplex()
    {
        return $this->isComplex;
    }

    /**
     * @param bool $isComplex
     * @return TypeBenefit
     */
    public function setIsComplex($isComplex)
    {
        $this->isComplex = $isComplex;
        return $this;
    }
}
