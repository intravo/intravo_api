<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use App\Filter\OrSearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * @ApiFilter(OrSearchFilter::class,
 *     properties={
 *        "code" : "ipartial",
 *        "title": "ipartial",
 *        "business": "ipartial",
 *        "customer": "ipartial",
 *        "benefit": "ipartial",
 *        "totalPriceHt": "ipartial",
 *        "etat": "ipartial"
 *     }
 * )
 * @ApiFilter(OrderFilter::class,
 *     properties={"code", "title", "business", "customer", "benefit", "totalPriceHt", "etat"},
 *     arguments={"orderParameterName"="order"}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\QuotationRepository")
 * @ApiResource(
 *     attributes={
 *       "order"={"id": "DESC"},
 *       "pagination_items_per_page"=20,
 *         "normalization_context"={
 *              "groups"={"quotation", "quotation-read"},
 *              "enable_max_depth" = "true",
 *         },
 *         "denormalization_context"={"groups"={"quotation", "quotation-write"}}
 *     },
 * )
 */
class Quotation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation", type="datetime")
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $creation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="validity", type="datetime")
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $validity;

    /**
     * @var string
     *
     * @ORM\Column(name="customerNote", type="text", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $customerNote;

    /**
     * @var string
     *
     * @ORM\Column(name="internalNote", type="text", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $internalNote;

    /**
     * @var float
     *
     * @ORM\Column(name="totalPriceHt", type="float", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $totalPriceHt;

    /**
     * @var float
     *
     * @ORM\Column(name="discountTotalPrice", type="float", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $discountTotalPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="totalPriceTva", type="float", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $totalPriceTva;

    /**
     * @var float
     *
     * @ORM\Column(name="tax", type="float", nullable=false, options={"default" : 0})
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $tax;

    /**
     * @var float
     *
     * @ORM\Column(name="TotalPriceTtc", type="float", nullable=true)
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     */
    private $totalPriceTtc;

    /**
     * @var Etat
     *
     * @ORM\ManyToOne(targetEntity="Etat", inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $etat;

    /**
     * @var PaymentCondition
     *
     * @ORM\ManyToOne(targetEntity="PaymentCondition", inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "business", "quotation_line", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $paymentCondition;

    /**
     * @var QuotationLine
     *
     * @ORM\OneToMany(targetEntity="QuotationLine", mappedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "business", "customer", "unit"})
     */
    private $quotationLines;

    /**
     * @var CGV
     *
     * @ORM\ManyToOne(targetEntity="CGV", inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "business", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $cgv;

    /**
     * @var TypeBenefit
     *
     * @ORM\ManyToOne(targetEntity="TypeBenefit", inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "business", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $benefit;

    /**
     * @var Business
     *
     * @ORM\ManyToOne(targetEntity="Business",  inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "quotation_line", "customer"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $business;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer",  inversedBy="quotation", cascade={"persist", "merge"})
     * @Groups({"quotation", "quotation_line"})
     * @ApiSubresource
     * @MaxDepth(1)
     */
    private $customer;

    /**
     * @var QuotationLineComplex
     *
     * @ORM\OneToMany(targetEntity="QuotationLineComplex", mappedBy="quotation")
     * @Groups({"quotation"})
     */
    private $quotationLineComplex;

    /**
     * Quotation constructor.
     */
    public function __construct()
    {
        $this->quotationLines = new ArrayCollection();
        $this->quotationLineComplex = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Quotation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Quotation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     *
     * @return Quotation
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set validity
     *
     * @param \DateTime $validity
     *
     * @return Quotation
     */
    public function setValidity($validity)
    {
        $this->validity = $validity;

        return $this;
    }

    /**
     * Get validity
     *
     * @return \DateTime
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * Set customerNote
     *
     * @param string $customerNote
     *
     * @return Quotation
     */
    public function setCustomerNote($customerNote)
    {
        $this->customerNote = $customerNote;

        return $this;
    }

    /**
     * Get customerNote
     *
     * @return string
     */
    public function getCustomerNote()
    {
        return $this->customerNote;
    }

    /**
     * Set internalNote
     *
     * @param string $internalNote
     *
     * @return Quotation
     */
    public function setInternalNote($internalNote)
    {
        $this->internalNote = $internalNote;

        return $this;
    }

    /**
     * Get internalNote
     *
     * @return string
     */
    public function getInternalNote()
    {
        return $this->internalNote;
    }

    /**
     * Set totalPriceHt
     *
     * @param float $totalPriceHt
     *
     * @return Quotation
     */
    public function setTotalPriceHt($totalPriceHt)
    {
        $this->totalPriceHt = $totalPriceHt;

        return $this;
    }

    /**
     * Get totalPriceHt
     *
     * @return float
     */
    public function getTotalPriceHt()
    {
        return $this->totalPriceHt;
    }

    /**
     * Set discountTotalPrice
     *
     * @param float $discountTotalPrice
     *
     * @return Quotation
     */
    public function setDiscountTotalPrice($discountTotalPrice)
    {
        $this->discountTotalPrice = $discountTotalPrice;

        return $this;
    }

    /**
     * Get discountTotalPrice
     *
     * @return float
     */
    public function getDiscountTotalPrice()
    {
        return $this->discountTotalPrice;
    }

    /**
     * Set totalPriceTva
     *
     * @param float $totalPriceTva
     *
     * @return Quotation
     */
    public function setTotalPriceTva($totalPriceTva)
    {
        $this->totalPriceTva = $totalPriceTva;

        return $this;
    }

    /**
     * Get totalPriceTva
     *
     * @return float
     */
    public function getTotalPriceTva()
    {
        return $this->totalPriceTva;
    }

    /**
     * Set totalPriceTtc
     *
     * @param float $totalPriceTtc
     *
     * @return Quotation
     */
    public function setTotalPriceTtc($totalPriceTtc)
    {
        $this->totalPriceTtc = $totalPriceTtc;

        return $this;
    }

    /**
     * Get totalPriceTtc
     *
     * @return float
     */
    public function getTotalPriceTtc()
    {
        return $this->totalPriceTtc;
    }

    /**
     * @return Etat
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param $etat
     * @return $this
     */
    public function setEtat(Etat $etat)
    {
        $this->etat = $etat;
        return $this;
    }

    /**
     * @return PaymentCondition
     */
    public function getPaymentCondition()
    {
        return $this->paymentCondition;
    }

    /**
     * @param PaymentCondition $paymentCondition
     * @return Quotation
     */
    public function setPaymentCondition(PaymentCondition $paymentCondition)
    {
        $this->paymentCondition = $paymentCondition;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuotationLines()
    {
        return $this->quotationLines;
    }

    /**
     * @param QuotationLine $quotationLine
     * @return $this
     */
    public function addQuotationLine(QuotationLine $quotationLine)
    {
        $this->quotationLines->add($quotationLine);
        $quotationLine->setQuotation($this);

        return $this;
    }

    /**
     * @param QuotationLine $quotationLine
     * @return $this
     */
    public function removeQuotationLine(QuotationLine $quotationLine)
    {
        $this->quotationLines->removeElement($quotationLine);

        return $this;
    }

    /**
     * @return CGV
     */
    public function getCgv()
    {
        return $this->cgv;
    }

    /**
     * @param CGV $cgv
     * @return Quotation
     */
    public function setCgv(CGV $cgv)
    {
        $this->cgv = $cgv;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBenefit()
    {
        return $this->benefit;
    }

    /**
     * @param TypeBenefit $benefit
     * @return $this
     */
    public function setBenefit(TypeBenefit $benefit)
    {
        $this->benefit = $benefit;
        return $this;
    }

    /**
     * @return QuotationLineComplex
     */
    public function getQuotationLineComplex()
    {
        return $this->quotationLineComplex;
    }

    /**
     * @return Business
     */
    public function getBusiness()
    {
        return $this->business;
    }

    /**
     * @param Business $business
     * @return Quotation
     */
    public function setBusiness(Business $business)
    {
        $this->business = $business;
        return $this;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return Quotation
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * @param float $tax
     * @return Quotation
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
        return $this;
    }
}