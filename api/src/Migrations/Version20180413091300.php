<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180413091300 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        /**
         * ========================
         *  Add Groupes
         * ========================
         */

        $this->addSql("INSERT INTO public.groupe( id, title ) VALUES (1,'in this moment !')");
        $this->addSql("INSERT INTO public.groupe( id, title ) VALUES (2,'PGM')");
        $this->addSql("INSERT INTO public.groupe( id, title ) VALUES (3,'Bismillah')");

        /**
         * ========================
         *  Add Customers
         * ========================
         */

        $this->addSql(" INSERT INTO public.customer(id, groupe_id,code, name,socialreason,siret,address,
                            addresscomplement, postalcode, city, phone, mobile, memo, isActive)
                        VALUES (1,1,'CLI0001','INTRAVO','INTRAVO & co','siret1','adress','','60290','apoil','0344804590','','memo',true ),
                               (2,2, 'CLI0002','64bit','e-64bit','siret2','adress1','compl','60290','a','034485590','','memo', false )
        ");

        /**
         * ========================
         *  Add ContactFunctions
         * ========================
         */

        $this->addSql("INSERT INTO public.contact_function(id, title) VALUES (1,'tank')");
        $this->addSql("INSERT INTO public.contact_function(id, title) VALUES (2,'Support')");
        $this->addSql("INSERT INTO public.contact_function(id, title) VALUES (3,'DPS')");
        $this->addSql("INSERT INTO public.contact_function(id, title) VALUES (4,'Heal')");

        /**
         * ========================
         *  Add Contacts
         * ========================
         */

        $this->addSql(" INSERT INTO public.contact(id, firstname, lastname, phone, mobilephone, email, customer_id, function_id) 
                        VALUES (1, 'jean-pierre','Pernot','0304506070','0654212121','jpp@email.com',1,1),
	                           (2, 'Célia','Dussaussois','5060804080','0245096070','cd@gmail.com',1,4),
	                           (3,'Ronan','troplong','7080708060','80906040456','rd@gmail.com',2,3),
	                           (4,'umai','sake','0340559060','','',2,2);
        ");

        /**
         * ========================
         *  Add Modules
         * ========================
         */

        $this->addSql("INSERT INTO public.module(id, title) VALUES (1, 'dashboard')");
        $this->addSql("INSERT INTO public.module(id, title) VALUES (2, 'crm')");
        $this->addSql("INSERT INTO public.module(id, title) VALUES (3, 'business-management')");
        $this->addSql("INSERT INTO public.module(id, title) VALUES (4, 'project-monitoring')");
        $this->addSql("INSERT INTO public.module(id, title) VALUES (5, 'indicators')");
        $this->addSql("INSERT INTO public.module(id, title) VALUES (6, 'administration')");

        /**
         * ========================
         *  Add Roles
         * ========================
         */

        $this->addSql("INSERT INTO public.role(id, title) VALUES (1, 'admin')");
        $this->addSql("INSERT INTO public.role(id, title) VALUES (2, 'user')");

        /**
         * ========================
         *  Add access module
         * ========================
         */

        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (1, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (2, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (3, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (4, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (5, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (6, 1);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (1, 2);");
        $this->addSql("INSERT INTO public.role_module(module_id, role_id) VALUES (2, 2);");


        /**
         * ========================
         *  Add Users
         * ========================
         */

        $this->addSql("
             INSERT INTO public.user_util(
	         id, 
	         email, 
	         enabled, 
	         salt, 
	         password, 
	         last_login, 
	         confirmation_token, 
	         password_requested_at, 
	         fullname, 
	         costperyear, 
	         username, 
	         username_canonical, 
	         email_canonical, 
	         roles,
	         role_id) VALUES (
	            5, 
	            'admin_role@intravo.com', 
	            true, 
	            null, 
	            '$2y$13\$C4AThYp2ROMfaZNMVEKSE.goMo1lBLVFoxjNdyEySfJ0N70vps4vS', 
	            null, 
	            null, 
	            null, 
	            'Admin Role', 
	            null, 
	            'admin_role', 
	            'admin_role', 
	            'admin_role@intravo.com', 
	            'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',
	            1
	         );   
        ");

        $this->addSql("
             INSERT INTO public.user_util(
	         id, 
	         email, 
	         enabled, 
	         salt, 
	         password, 
	         last_login, 
	         confirmation_token, 
	         password_requested_at, 
	         fullname, 
	         costperyear, 
	         username, 
	         username_canonical, 
	         email_canonical, 
	         roles,
	         role_id) VALUES (
	            6, 
	            'user_role@intravo.com', 
	            true, 
	            null, 
	            '$2y$13\$dtQkOMLYmpOP2h3s6Lf2au//Nc8knvGClyNfFtZz//yj9TlOtTAky', 
	            null, 
	            null, 
	            null, 
	            'User Role', 
	            null, 
	            'user_role', 
	            'user_role', 
	            'user_role@intravo.com', 
	            'a:1:{i:0;s:10:\"ROLE_ADMIN\";}',
	            2
	         );   
        ");

        /**
         * ========================
         *  Add etats
         * ========================
         */

        $this->addSql("INSERT INTO public.etat( id, title) VALUES (1, 'Etat 1');");
        $this->addSql("INSERT INTO public.etat( id, title) VALUES (2, 'Etat 2');");
        $this->addSql("INSERT INTO public.etat( id, title) VALUES (3, 'Etat 3');");
        $this->addSql("INSERT INTO public.etat( id, title) VALUES (4, 'Etat 4');");

        /**
         * ========================
         *  Add type_benefit
         * ========================
         */

        $this->addSql("INSERT INTO public.type_benefit( id, title) VALUES (1, 'Prestations 1');");
        $this->addSql("INSERT INTO public.type_benefit( id, title) VALUES (2, 'Prestations 2');");
        $this->addSql("INSERT INTO public.type_benefit( id, title) VALUES (3, 'Prestations 3');");
        $this->addSql("INSERT INTO public.type_benefit( id, title) VALUES (4, 'Prestations 4');");

        /**
         * ========================
         *  Add Payment condition
         * ========================
         */
        $this->addSql("INSERT INTO public.payment_condition(
                        id, title)
                        VALUES (1, 'CB'),
                        (2,'virement'),
                        (3,'Nature'),
                        (4, 'PayPal'),
                        (5, 'Charlie');");

        /**
         * ========================
         *  Add cgv
         * ========================
         */
        $this->addSql("INSERT INTO public.cgv( id, title)
                       VALUES (1, 'CGV 1'),
                              (2, 'CGV 2'),
                              (3, 'CGV 3'),
                              (4, 'CGV 4');");

        /**
         * ========================
         *  Add business
         * ========================
         */
        $this->addSql(" INSERT INTO  public.business(id, code, libelle, note, file, customer_id, com_contact_id, 
                                                     tech_contact_id, com_user_id, tech_user_id)
                        VALUES 
                            (1, 'AFF01', 'Affaire1', 'note', 'file', 1, 1, 2, 5, 6),
                            (2, 'AFF02', 'Affaire2', 'note2', 'file2', 2, 3, 4, 5, 6),
                            (3, 'AFF03', 'Affaire3', 'note3', 'file3', 1, 1, 2, 5, 6),
                            (4, 'AFF04', 'Affaire4', 'note4', 'file4', 2, 3, 4, 5, 6);
        ");

        /**
         * ========================
         *  Add Quotation
         * ========================
         */

        $this->addSql("
            INSERT INTO public.quotation(
	        id, code, title, creation, 
	        validity, customernote, internalnote, 
	        totalpriceht, discounttotalprice, 
	        totalpricetva, totalpricettc, etat_id, 
	        payment_condition_id, cgv_id, benefit_id, business_id, customer_id, tax)
	        VALUES (
                1, 
                '12ZEA', 
                'Devis n°1', 
                'Fri May 18 2018 10:44:40', 
                'Fri May 18 2018 10:44:40', 
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 
                10000, 
                9000,
                9000, 
                11000, 
                1, 
                1, 
                1, 
                1,
                1,
                1,
                20
	        );
        ");

        $this->addSql("
            INSERT INTO public.quotation(
	        id, code, title, creation, 
	        validity, customernote, internalnote, 
	        totalpriceht, discounttotalprice, 
	        totalpricetva, totalpricettc, etat_id, 
	        payment_condition_id, cgv_id, benefit_id, business_id, customer_id, tax)
	        VALUES (
                2, 
                '54ZEA', 
                'Devis n°2', 
                'Fri May 18 2018 10:44:40', 
                'Fri May 18 2018 10:44:40', 
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 
                10000, 
                9000,
                9000, 
                11000, 
                2, 
                2, 
                2, 
                2,
                2,
                1,
                20
	        );
        ");

        $this->addSql("
            INSERT INTO public.quotation(
	        id, code, title, creation, 
	        validity, customernote, internalnote, 
	        totalpriceht, discounttotalprice, 
	        totalpricetva, totalpricettc, etat_id, 
	        payment_condition_id, cgv_id, benefit_id, business_id, customer_id, tax)
	        VALUES (
                3, 
                '54ZDD', 
                'Devis n°3', 
                'Fri May 18 2018 10:44:40', 
                'Fri May 18 2018 10:44:40',  
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 
                10000, 
                9000,
                000, 
                11000, 
                3, 
                3, 
                3, 
                3,
                3,
                2,
                20
	        );
        ");

        $this->addSql("
            INSERT INTO public.quotation(
	        id, code, title, creation, 
	        validity, customernote, internalnote, 
	        totalpriceht, discounttotalprice, 
	        totalpricetva, totalpricettc, etat_id, 
	        payment_condition_id, cgv_id, benefit_id, business_id, customer_id, tax)
	        VALUES (
                4, 
                '54WDD', 
                'Devis n°4', 
                'Fri May 18 2018 10:44:40', 
                'Fri May 18 2018 10:44:40', 
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 
                10000, 
                9000,
                000, 
                11000, 
                1, 
                1, 
                1, 
                1,
                4,
                2,
                20
	        );
        ");


        /**
         * ========================
         *  add units
         * ========================
         */
        $this->addSql("INSERT INTO public.unit(id, title)
	                   VALUES (1, 'Unite 1'),
	                          (2, 'Unite 2');
	    ");

        /**
         * ========================
         *  add quotation_lines
         * ========================
         */

        $this->addSql("
            INSERT INTO public.quotation_line(
	        id, designation, quantity, priceht, 
	        position, isoptional, quotation_id, unit_id, isgift, istitle)
	        VALUES (1, 'Designation 1', 1, 200, 1, false, 1, 1, false, false),
	               (2, 'Designation 2', 1, 300, 2, true, 1, 2, false, false),
	               (3, 'Designation 3', 1, 400, 1, false, 2, 1, false, false),
	               (4, 'Designation 4', 1, 500, 2, true, 2, 2, false, false), 
	               (5, 'Designation 3', 1, 600, 1, false, 3, 1, false, false),
	               (6, 'Designation 4', 1, 700, 2, true, 3, 2, false, false), 
	               (7, 'Designation 3', 1, 600, 1, false, 4, 1, false, false),
	               (8, 'Designation 4', 1, 700, 2, true, 4, 2, false, false);
        ");
        /**
         * ========================
         *  add quotation_line_complex
         * ========================
         */

        $this->addSql("
            INSERT INTO public.quotation_line_complex(id, titleclient, culomn, t_jm, user_id, quotation_id)
                VALUES (1, 'culcomn 1',1, 25, 5,4),
                    (2, 'culomn 2', 2, 26, 5,4),
                    (3, 'culomn 3', 3, 27, 5,4),
                    (4, 'culomn 4', 4, 28, 5,4),
                    (5, 'culomn 5', 5, 29, 5,4),
                    (6, 'culomn 1', 1, 35, 5,1),
                    (7, 'culomn 2', 2, 36, 5,1),
                    (8, 'culomn 3', 3, 37, 5,1),
                    (9, 'culomn 4', 4, 38, 5,1),
                    (10, 'culomn 5', 5, 39, 5,1);
        ");

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
