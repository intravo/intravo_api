<?php

namespace App\Repository;

use App\Entity\PaymentCondition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PaymentCondition|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentCondition|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentCondition[]    findAll()
 * @method PaymentCondition[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentConditionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PaymentCondition::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
