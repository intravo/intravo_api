<?php

namespace App\Repository;

use App\Entity\QuotationLineComplex;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuotationLineComplex|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuotationLineComplex|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuotationLineComplex[]    findAll()
 * @method QuotationLineComplex[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuotationLineComplexRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuotationLineComplex::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('q')
            ->where('q.something = :value')->setParameter('value', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
