<?php

namespace App\Repository;

use App\Entity\TypeQuotation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeQuotation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeQuotation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeQuotation[]    findAll()
 * @method TypeQuotation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeQuotationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeQuotation::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
