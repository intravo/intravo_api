<?php

namespace App\Repository;

use App\Entity\ModuleAccess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ModuleAccess|null find($id, $lockMode = null, $lockVersion = null)
 * @method ModuleAccess|null findOneBy(array $criteria, array $orderBy = null)
 * @method ModuleAccess[]    findAll()
 * @method ModuleAccess[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleAccessRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ModuleAccess::class);
    }

//    /**
//     * @return ModuleAccess[] Returns an array of ModuleAccess objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ModuleAccess
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
