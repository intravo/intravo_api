<?php

namespace App\Repository;

use App\Entity\ContactFunction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ContactFunction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactFunction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactFunction[]    findAll()
 * @method ContactFunction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactFunctionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ContactFunction::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
