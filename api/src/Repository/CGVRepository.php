<?php

namespace App\Repository;

use App\Entity\CGV;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CGV|null find($id, $lockMode = null, $lockVersion = null)
 * @method CGV|null findOneBy(array $criteria, array $orderBy = null)
 * @method CGV[]    findAll()
 * @method CGV[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CGVRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CGV::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('c')
            ->where('c.something = :value')->setParameter('value', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
