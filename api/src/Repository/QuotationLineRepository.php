<?php

namespace App\Repository;

use App\Entity\QuotationLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method QuotationLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuotationLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuotationLine[]    findAll()
 * @method QuotationLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuotationLineRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, QuotationLine::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('q')
            ->where('q.something = :value')->setParameter('value', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
