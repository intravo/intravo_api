<?php

namespace App\Repository;

use App\Entity\PeriodNoTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PeriodNoTime|null find($id, $lockMode = null, $lockVersion = null)
 * @method PeriodNoTime|null findOneBy(array $criteria, array $orderBy = null)
 * @method PeriodNoTime[]    findAll()
 * @method PeriodNoTime[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeriodNoTimeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PeriodNoTime::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('p')
            ->where('p.something = :value')->setParameter('value', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
