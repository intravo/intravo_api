<?php

namespace App\Repository;

use App\Entity\TypeBenefit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeBenefit|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeBenefit|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeBenefit[]    findAll()
 * @method TypeBenefit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeBenefitRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeBenefit::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('t')
            ->where('t.something = :value')->setParameter('value', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
