## Lancer le serveur du projet
    cd intravo_api
    docker-compose up -d
    docker-compose run api

## Checker les containers qui run:
    docker stats

## Arrêter un container : 
    docker stop [nom du container]

## Supprimer un container : 
    docker rm [nom du container]

## Pour exécuter une commande sf : 
    docker-compose exec php bin/console commande

## Si tu veux use composer c’est : 
    docker-compose exec php composer install

## Accès de l'interface web de la bdd : localhost:5050
    Login root:root
    
## Migrer la base de données
    make dsu
    
## Insérer les fake datas
    make dmm